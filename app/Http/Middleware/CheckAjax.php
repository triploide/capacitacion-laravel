<?php

namespace App\Http\Middleware;

use Closure;

class CheckAjax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $admin = null)
    {
        if ($admin != 'admin') {
            if (!$request->ajax()) {
                return abort(500);
            }
        }

        return $next($request);
    }
}
