<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller
{
    public function add($id)
    {
        session()->push('cart', $id);

        session()->flash('message', 'El producto se agregó al carrito');

        return redirect('admin/products');
    	//dd(session()->all());
    }

    public function remove($id)
    {
    	# code...
    }

    public function checkout()
    {
        $total = 0;
        foreach (session()->get('cart') as $id) {
            $product = Product::find($id);
            $total += $product->price;
        }
    	return $total;
    }
}
