<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Productos</title>
	<link rel="stylesheet" href="/css/app.css">
	<style>
		body {
			padding: 40px;
		}
	</style>
</head>
<body>
	<nav class="navbar">
			<ul>
				<li class="nav-item"><a href="/admin/products">Products</a></li>
				@if(\Auth::check())
					<li class="nav-item"><a href="/logout">Logout</a></li>
				@else
					<li class="nav-item"><a href="/login">Login</a></li>
				@endif
			</ul>
		</nav>

	<div class="container">
		<h1>Productos</h1>

		
		@if(session()->has('message'))
			<div class="alert alert-success">
				{{ session()->get('message') }}
			</div>
		@endif		

		<section id="products">
			@foreach($listado as $item)
				<p>{{ $item->name }} <a href="/cart/add/{{$item->id}}">Agregar</a></p>
			@endforeach
		</section>

		<p id="verMas"><a href="ver-mas">Ver más</a></p>
		

		<p><a href="/cart/checkout">Total</a></p>

		{{ $listado->links() }}
	</div>

	<script src="/js/app.js"></script>

	<script>
		var page = 1;
		$('#verMas a').on('click', function (e) {
			e.preventDefault();
			
			page++;
			
			$.ajax({
				url: '/admin/products',
				data: {
					page: page
				},
				success: function (response) {
					if (response.last_page == page) $('#verMas').remove();

					var html = '';
					response.data.map(function (element) {
						html += '<p>' + element.name + '</p>';
					});
					$('#products').append(html);
				}
			})
		})
	</script>

	

</body>
</html>