<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Crear Producto</title>
	<link rel="stylesheet" href="/css/app.css">
	<style>
		body {
			padding: 40px;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Crear Producto</h1>
		<form action="/admin/products" method="post">
			@csrf
			<div class="form-group">
				<label for="name">Nombre</label><br>
				<input class="form-control" type="text" name="name" value="">
			</div>
			<div class="form-group">
				<label for="price">Precio</label><br>
				<input class="form-control" type="text" name="price" value="">
			</div>
			<div class="form-group">
				<input class="btn btn-primary" type="submit" name="enviador" value="Enviar">
			</div>
		</form>

		@if(count($errors))
			<div>
				@foreach($errors->all() as $error)
					<p>{{ $error }}</p>
				@endforeach
			</div>
		@endif
	</div>
</body>
</html>