<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>React</title>
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
	<h1>React</h1>
	<section id="example">
		
	</section>
	<script src="/js/app.js"></script>
</body>
</html>