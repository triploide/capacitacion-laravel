<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('admin/categorias', 'CategoryController');

Route::resource('admin/products', 'ProductController');

Route::get('cart/add/{id}', 'CartController@add');
Route::get('cart/checkout', 'CartController@checkout');

//Route::view('admin/categorias', 'categories');

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::view('api', 'api')->middleware('checkAjax:admin');

Route::view('react', 'react');

/*
\Auth::check();
\Auth::guard('web')->check();
\Auth::guard('client')->check();



\Auth::user()->name;

\Auth::guard('client')->user()->name;

\Auth::attemp(['email' => 'valor', 'pass' => 'pass']);
*/
